import torch
import torch.nn as nn
import efficientnet_pytorch as efp
from typing import List, Dict, Callable, Any, Tuple
from transformers import T5ForConditionalGeneration, T5Tokenizer, AdamW
from transformers import get_linear_schedule_with_warmup, AdamW
from transformers import MobileBertTokenizer, MobileBertForQuestionAnswering, MobileBertModel

from transformers import AutoTokenizer, AutoModelForQuestionAnswering


class dqvaModel(nn.Module):
    def __init__(self, t5_model: str = "t5-small",
                 eff_model: str = "efficientnet-b0",
                 img_embedded_shape: Tuple[int] = (1, 1),
                 max_length: int = 128,
                 t5_embed_size: int = 512):
        super().__init__()

        self.effNet     = efp.EfficientNet.from_pretrained(eff_model)
        self.max_length = max_length
        self.t5_freeze  = T5ForConditionalGeneration.from_pretrained(t5_model).eval()
        self.t5_train   = T5ForConditionalGeneration.from_pretrained(t5_model)
        self.eff_reduction = 'reduction_5'

        inChannels = self.effChannels(eff_model)
        outChannels = t5_embed_size

        for p in self.t5_freeze.parameters():
            p.requires_grad = False

        for p in self.effNet.parameters():
            p.requires_grad = False

        self.build_embed = nn.Sequential(
            nn.AdaptiveAvgPool2d(img_embedded_shape),
            nn.Conv2d(inChannels, outChannels, kernel_size=1))


    def effChannels(self, eff_model: str) -> int:
        # number of channels at reduction 4
        values = {"efficientnet-b0": 112,
                  "efficientnet-b1": 112,
                  "efficientnet-b2": 120,
                  "efficientnet-b3": 136,
                  "efficientnet-b4": 160,
                  "efficientnet-b5": 176,
                  "efficientnet-b6": 200,
                  "efficientnet-b7": 224,
                  }
        # number of channels at reduction 5
        values = {"efficientnet-b0": 1280,
                  "efficientnet-b1": 1280,
                  "efficientnet-b2": 1408,
                  "efficientnet-b3": 1536,
                  "efficientnet-b4": 1792,
                  "efficientnet-b5": 2048,
                  "efficientnet-b6": 2304,
                  "efficientnet-b7": 2560,
                  }
        return values[eff_model]

    def image_embed(self, images:torch.Tensor)->torch.Tensor:
#        with torch.no_grad():
        self.effNet.eval()
        temp  = self.effNet.extract_endpoints(images)[self.eff_reduction]

        embed = self.build_embed(temp.detach())

        # NCHW -> N(HW)C = NLE
        N, C, H, W = embed.shape
        embed = embed.permute(0, 2, 3, 1).contiguous().view(N, H*W, C)

        return embed

    def forward(self, batch:Dict[str,Any]):
        #img, question_ids, question_mask, ocr_embeddings, answer_ids, answers = batch

        images       = batch['image']
        question_ids = batch['question_ids']
        question_msk = batch['question_msk']
        answers_ids  = batch['answer_ids']
        answers_msk  = batch['answer_msk']
        ocr_ids      = batch['ocr_cat_ids']
        ocr_msk      = batch['ocr_cat_msk']

        # with torch.no_grad():
        self.t5_freeze.eval()
        question_embeds = self.t5_freeze.encoder(
            input_ids=question_ids, 
            attention_mask=question_msk, 
            return_dict=True).last_hidden_state

        ocr_embeds = self.t5_freeze.encoder(
            input_ids=ocr_ids,
            attention_mask=ocr_msk, 
            return_dict=True).last_hidden_state

        img_embeds = self.image_embed(images)

        input_embeds = torch.cat([img_embeds, ocr_embeds, question_embeds], dim=1)

        if self.training:
            outputs = self.t5_train(
                inputs_embeds=input_embeds, 
                labels=answers_ids,
                return_dict=True
            )
            return outputs.loss
 
        else:
            return self.generate(input_embeds)
 
    def generate(self, inputs_embeds):
        decoded_ids = torch.full(
            size=(inputs_embeds.shape[0], 1), 
            fill_value=self.t5_train.config.decoder_start_token_id, 
            dtype=torch.long
        ).to(inputs_embeds.device)
 
        encoder_hidden_states = self.t5_train.get_encoder()(
            inputs_embeds=inputs_embeds
        )
 
        for step in range(self.max_length):
            logits = self.t5_train(decoder_input_ids=decoded_ids,
                             encoder_outputs=encoder_hidden_states)[0]
            next_token_logits = logits[:, -1, :]
            next_token_id = next_token_logits.argmax(1).unsqueeze(-1)
            if torch.eq(next_token_id[:, -1], 1).all():
                break
 
            decoded_ids = torch.cat([decoded_ids, next_token_id], dim=-1)
        return decoded_ids

class qaModel(nn.Module):
    def __init__(self, hf_model:str, tokenizer, emb_size:int, max_length:int=512) -> None:
        super().__init__()
        self.max_length = max_length
        self.tokenizer  = tokenizer
        self.bertLike   = MobileBertForQuestionAnswering\
                         .from_pretrained(hf_model, return_dict=True)
        # self.fc = nn.Linear(2 * emb_size, emb_size//4)
        # self.ln = nn.LayerNorm(emb_size//4)
    
    def generate(self, batch)->Dict[str,List[str]]:
        trues, preds = [], []
        question = batch['question']
        context  = batch['context']
        answer   = batch['answer']
        device   = batch['input_ids'].device
        for q, c, a in zip(question, context, answer):
            predict_inputs = self.tokenizer.encode(
                                                q, c,
                                                max_length=self.max_length,
                                                truncation=True,
                                            )
            sep_index = predict_inputs.index(self.tokenizer.sep_token_id)
            num_seg_a = sep_index + 1
            num_seg_b = len(predict_inputs) - num_seg_a
            segment_ids = [0]*num_seg_a + [1]*num_seg_b
            assert len(segment_ids) == len(predict_inputs)
            self.bertLike.eval()
            with torch.no_grad():
                inpt_ids = torch.Tensor([predict_inputs]).long().to(device)
                type_ids = torch.Tensor([segment_ids]).long().to(device)
                output = self.bertLike(inpt_ids, token_type_ids=type_ids)

            start_logits = output['start_logits']
            end_logits   = output['end_logits']
            answer_start = torch.argmax(start_logits)
            answer_end   = torch.argmax(end_logits)
            tokens = self.tokenizer.convert_ids_to_tokens(predict_inputs)
            pred_answers = tokens[answer_start]
    
            for i in range(answer_start + 1, answer_end + 1):
                if tokens[i][0:2] == '##':
                    pred_answers += tokens[i][2:]
                else:
                    pred_answers += ' ' + tokens[i]
            # print('pred_answers:',pred_answers)
            # print('answer:',answer)
            preds.append(pred_answers)
            trues.append(a)
        return {"preds":preds,"trues":trues}

    def forward(self, batch:Dict)->Any:
        if self.training:
            #print('batch.input_ids.shape:',batch['input_ids'].shape)
            ret = self.bertLike(input_ids=batch['input_ids'],
                                token_type_ids=batch['segment_ids'],
                                attention_mask=batch['attn_mask'],
                                start_positions=batch['start'],
                                end_positions=batch['end'])
            return ret['loss']
        else:
            ret = self.generate(batch)
            return ret

class qaModelBert(nn.Module):
    def __init__(self, hf_model:str, tokenizer, emb_size:int, max_length:int=512) -> None:
        super().__init__()
        self.max_length = max_length
        self.tokenizer  = tokenizer
        self.bertLike   = AutoModelForQuestionAnswering.from_pretrained(hf_model, return_dict=True)

    def generate(self, batch)->Dict[str,List[str]]:
        trues, preds = [], []
        question = batch['question']
        context  = batch['context']
        answer   = batch['answer']
        device   = batch['input_ids'].device
        for q, c, a in zip(question, context, answer):
            predict_inputs = self.tokenizer.encode(
                                                q, c,
                                                max_length=self.max_length,
                                                truncation=True,
                                            )
            sep_index = predict_inputs.index(self.tokenizer.sep_token_id)
            num_seg_a = sep_index + 1
            num_seg_b = len(predict_inputs) - num_seg_a
            segment_ids = [0]*num_seg_a + [1]*num_seg_b
            assert len(segment_ids) == len(predict_inputs)
            self.bertLike.eval()
            with torch.no_grad():
                inpt_ids = torch.Tensor([predict_inputs]).long().to(device)
                type_ids = torch.Tensor([segment_ids]).long().to(device)
                output = self.bertLike(inpt_ids, token_type_ids=type_ids)

            start_logits = output['start_logits']
            end_logits   = output['end_logits']
            answer_start = torch.argmax(start_logits)
            answer_end   = torch.argmax(end_logits)
            tokens = self.tokenizer.convert_ids_to_tokens(predict_inputs)
            pred_answers = tokens[answer_start]
    
            for i in range(answer_start + 1, answer_end + 1):
                if tokens[i][0:2] == '##':
                    pred_answers += tokens[i][2:]
                else:
                    pred_answers += ' ' + tokens[i]
            # print('pred_answers:',pred_answers)
            # print('answer:',answer)
            preds.append(pred_answers)
            trues.append(a)
        return {"preds":preds,"trues":trues}

    def forward(self, batch:Dict)->Any:
        if self.training:
            #print('batch.input_ids.shape:',batch['input_ids'].shape)
            ret = self.bertLike(input_ids=batch['input_ids'],
                                token_type_ids=batch['segment_ids'],
                                attention_mask=batch['attn_mask'],
                                start_positions=batch['start'],
                                end_positions=batch['end'])
            return ret['loss']
        else:
            ret = self.generate(batch)
            return ret


# class qaModelVis(nn.Module):
#     def __init__(self, hf_model:str, tokenizer, emb_size:int=512, 
#                  eff_model: str = "efficientnet-b0",
#                  max_length:int=512) -> None:
#         super().__init__()
#         self.max_length = max_length
#         self.tokenizer  = tokenizer
#         self.bertLike   = MobileBertForQuestionAnswering\
#                          .from_pretrained(hf_model, return_dict=True)
#         self.effNet     = efp.EfficientNet.from_pretrained(eff_model)
#         self.avgpool    = nn.AdaptiveAvgPool2d(1)
#         self.conv1x1    = nn.Conv2d(self.effCchanels(eff_model), emb_size, kernel_size=1)
#         self.eff_reduction = 'reduction_5'
#         for p in self.effNet.parameters():
#             p.requires_grad = False

#     def effChannels(self, eff_model: str) -> int:
#         # number of channels at reduction 4
#         values = {"efficientnet-b0": 112,
#                   "efficientnet-b1": 112,
#                   "efficientnet-b2": 120,
#                   "efficientnet-b3": 136,
#                   "efficientnet-b4": 160,
#                   "efficientnet-b5": 176,
#                   "efficientnet-b6": 200,
#                   "efficientnet-b7": 224,
#                   }
#         # number of channels at reduction 5
#         values = {"efficientnet-b0": 1280,
#                   "efficientnet-b1": 1280,
#                   "efficientnet-b2": 1408,
#                   "efficientnet-b3": 1536,
#                   "efficientnet-b4": 1792,
#                   "efficientnet-b5": 2048,
#                   "efficientnet-b6": 2304,
#                   "efficientnet-b7": 2560,
#                   }
#         return values[eff_model]

#     def image_embed(self, images:torch.Tensor)->torch.Tensor:
# #        with torch.no_grad():
#         self.effNet.eval()
#         temp  = self.effNet.extract_endpoints(images)[self.eff_reduction]

#         embed = self.build_embed(temp.detach())

#         # NCHW -> N(HW)C = NLE
#         N, C, H, W = embed.shape
#         embed = embed.permute(0, 2, 3, 1).contiguous().view(N, H*W, C)

#         return embed

#     def buildImageEmbeds(self, image:torch.Tensor)->torch.Tensor:
#         y = self.effNet.extract_endpoints(image)[self.eff_reduction]
#         y = self.avgpool(y)
#         y = self.conv1x1(y)
#         N, C, H, W = y.shape
#         embed = y.permute(0, 2, 3, 1).contiguous().view(N, H*W, C)
#         return
#     def generate(self, batch)->Dict[str,List[str]]:
#         trues, preds = [], []
#         question = batch['question']
#         context  = batch['context']
#         answer   = batch['answer']
#         device   = batch['input_ids'].device
#         for q, c, a in zip(question, context, answer):
#             predict_inputs = self.tokenizer.encode(
#                                                 q, c,
#                                                 max_length=self.max_length,
#                                                 truncation=True,
#                                             )
#             sep_index = predict_inputs.index(self.tokenizer.sep_token_id)
#             num_seg_a = sep_index + 1
#             num_seg_b = len(predict_inputs) - num_seg_a
#             segment_ids = [0]*num_seg_a + [1]*num_seg_b
#             assert len(segment_ids) == len(predict_inputs)
#             self.bertLike.eval()
#             with torch.no_grad():
#                 inpt_ids = torch.Tensor([predict_inputs]).long().to(device)
#                 type_ids = torch.Tensor([segment_ids]).long().to(device)
#                 output = self.bertLike(inpt_ids, token_type_ids=type_ids)

#             start_logits = output['start_logits']
#             end_logits   = output['end_logits']
#             answer_start = torch.argmax(start_logits)
#             answer_end   = torch.argmax(end_logits)
#             tokens = self.tokenizer.convert_ids_to_tokens(predict_inputs)
#             pred_answers = tokens[answer_start]
    
#             for i in range(answer_start + 1, answer_end + 1):
#                 if tokens[i][0:2] == '##':
#                     pred_answers += tokens[i][2:]
#                 else:
#                     pred_answers += ' ' + tokens[i]
#             # print('pred_answers:',pred_answers)
#             # print('answer:',answer)
#             preds.append(pred_answers)
#             trues.append(a)
#         return {"preds":preds,"trues":trues}

#     def forward(self, batch:Dict)->Any:
#         if self.training:
#             #print('batch.input_ids.shape:',batch['input_ids'].shape)
#             ret = self.bertLike(input_ids=batch['input_ids'],
#                                 token_type_ids=batch['segment_ids'],
#                                 attention_mask=batch['attn_mask'],
#                                 start_positions=batch['start'],
#                                 end_positions=batch['end'])
#             total_loss = None
#             loss_fct = nn.CrossEntropyLoss()
#             return ret['loss']
#         else:
#             ret = self.generate(batch)
#             return ret


class qaModelVis(nn.Module):
    def __init__(self, hf_model:str, tokenizer, emb_size:int=512, 
                 eff_model: str = "efficientnet-b0",
                 max_length:int=512) -> None:
        super().__init__()
        self.max_length = max_length
        self.tokenizer  = tokenizer
        self.bertLike   = MobileBertForQuestionAnswering\
                         .from_pretrained(hf_model, return_dict=True)
        self.effNet     = efp.EfficientNet.from_pretrained(eff_model)
        self.avgpool    = nn.AdaptiveAvgPool2d(1)
        self.conv1x1    = nn.Conv2d(self.effCchanels(eff_model), emb_size, kernel_size=1)
        self.eff_reduction = 'reduction_5'
        for p in self.effNet.parameters():
            p.requires_grad = False

    def effChannels(self, eff_model: str) -> int:
        # number of channels at reduction 4
        values = {"efficientnet-b0": 112,
                  "efficientnet-b1": 112,
                  "efficientnet-b2": 120,
                  "efficientnet-b3": 136,
                  "efficientnet-b4": 160,
                  "efficientnet-b5": 176,
                  "efficientnet-b6": 200,
                  "efficientnet-b7": 224,
                  }
        # number of channels at reduction 5
        values = {"efficientnet-b0": 1280,
                  "efficientnet-b1": 1280,
                  "efficientnet-b2": 1408,
                  "efficientnet-b3": 1536,
                  "efficientnet-b4": 1792,
                  "efficientnet-b5": 2048,
                  "efficientnet-b6": 2304,
                  "efficientnet-b7": 2560,
                  }
        return values[eff_model]

    def image_embed(self, images:torch.Tensor)->torch.Tensor:
#        with torch.no_grad():
        self.effNet.eval()
        temp  = self.effNet.extract_endpoints(images)[self.eff_reduction]

        embed = self.build_embed(temp.detach())

        # NCHW -> N(HW)C = NLE
        N, C, H, W = embed.shape
        embed = embed.permute(0, 2, 3, 1).contiguous().view(N, H*W, C)

        return embed

    def buildImageEmbeds(self, image:torch.Tensor)->torch.Tensor:
        y = self.effNet.extract_endpoints(image)[self.eff_reduction]
        y = self.avgpool(y)
        y = self.conv1x1(y)
        N, C, H, W = y.shape
        embed = y.permute(0, 2, 3, 1).contiguous().view(N, H*W, C)
        return
    def generate(self, batch)->Dict[str,List[str]]:
        trues, preds = [], []
        question = batch['question']
        context  = batch['context']
        answer   = batch['answer']
        device   = batch['input_ids'].device
        for q, c, a in zip(question, context, answer):
            predict_inputs = self.tokenizer.encode(
                                                q, c,
                                                max_length=self.max_length,
                                                truncation=True,
                                            )
            sep_index = predict_inputs.index(self.tokenizer.sep_token_id)
            num_seg_a = sep_index + 1
            num_seg_b = len(predict_inputs) - num_seg_a
            segment_ids = [0]*num_seg_a + [1]*num_seg_b
            assert len(segment_ids) == len(predict_inputs)
            self.bertLike.eval()
            with torch.no_grad():
                inpt_ids = torch.Tensor([predict_inputs]).long().to(device)
                type_ids = torch.Tensor([segment_ids]).long().to(device)
                output = self.bertLike(inpt_ids, token_type_ids=type_ids)

            start_logits = output['start_logits']
            end_logits   = output['end_logits']
            answer_start = torch.argmax(start_logits)
            answer_end   = torch.argmax(end_logits)
            tokens = self.tokenizer.convert_ids_to_tokens(predict_inputs)
            pred_answers = tokens[answer_start]
    
            for i in range(answer_start + 1, answer_end + 1):
                if tokens[i][0:2] == '##':
                    pred_answers += tokens[i][2:]
                else:
                    pred_answers += ' ' + tokens[i]
            # print('pred_answers:',pred_answers)
            # print('answer:',answer)
            preds.append(pred_answers)
            trues.append(a)
        return {"preds":preds,"trues":trues}

    def forward(self, batch:Dict)->Any:
        if self.training:
            #print('batch.input_ids.shape:',batch['input_ids'].shape)
            ret = self.bertLike(input_ids=batch['input_ids'],
                                token_type_ids=batch['segment_ids'],
                                attention_mask=batch['attn_mask'],
                                start_positions=batch['start'],
                                end_positions=batch['end'])
            total_loss = None
            loss_fct = nn.CrossEntropyLoss()
            return ret['loss']
        else:
            ret = self.generate(batch)
            return ret


def test_qa():
    hf_model = 'mrm8488/mobilebert-uncased-finetuned-squadv2'
    #hf_model = 'google/mobilebert-uncased'
    tokenizer = MobileBertTokenizer.from_pretrained(hf_model)
    net = qaModel(hf_model, tokenizer, emb_size=512)
    print(net)

if __name__ == "__main__":
    test_qa()
