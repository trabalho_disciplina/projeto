from dataclasses import dataclass, field
import os
import torch.utils.data as data
from time import time
from typing import Any,Dict,List,Tuple,Union
import json 
from PIL import Image
import PIL
import torchvision.transforms.functional as TF

@dataclass
class docItem:
    img_path:str
    ocr_path:str
    questionId:int
    docId:int
    question:str
    answers:List[str] = field(default_factory=list)

@dataclass
class ocrItem:
    text:List[str]
    bbox:List[List[int]]
    width:int
    height:int
    clockwiseOrientation:float

@dataclass
class questionAnswer:
    question:str
    questionId:int
    answers:List[str] = field(default_factory=list)
        
@dataclass
class documentQueries:
    img_path:str
    ocr_path:str
    docId:int
    queries:List[questionAnswer]
    ocr:ocrItem

def _parseJson(base_path:str, jsonFile:str, kind:str)->List[docItem]:
    with open(jsonFile,"r") as fp:
        js = json.load(fp)
    assert js['dataset_split'] == kind

    items: List[docItem] = []
    for item in js['data']:
        t = docItem(img_path=os.path.join(base_path, item['image']),
                    ocr_path=os.path.join(base_path, item['image']\
                               .replace(".png", ".json")\
                               .replace("documents","ocr_results")),
                    docId=item['docId'],                        
                    questionId=item['questionId'],
                    question=item['question'],
                    answers=item['answers'])
        items.append(t)
    return items

def _parse_ocr_json(ocr_path:str)->ocrItem:
    assert os.path.isfile(ocr_path)

    with open(ocr_path, "r") as fp:
        js = json.load(fp)

    text_list:List[str] = []
    bbox_list:List[List[int]] = []

    for it in  js['recognitionResults'][0]['lines']:
        text_list.append(it['text'])
        bbox_list.append(it['boundingBox'])

    if 'clockwiseOrientation' in js['recognitionResults'][0]:
        orientation = js['recognitionResults'][0]['clockwiseOrientation']
    else:
        orientation = 360.0

    ret = ocrItem(text=text_list,
                  bbox=bbox_list,
                  width=js['recognitionResults'][0]['width'], 
                  height=js['recognitionResults'][0]['height'],
                  clockwiseOrientation=orientation)
    return ret

def buildOcrDocItems(base_path:str, kind:str)->List[documentQueries]:
    """ 
        Anchor questions/answers by images
    """
    assert os.path.isdir(base_path), f"got{os.path.isdir(base_path)}"
    assert kind in ["train","test","val"], f"got {kind}"
    ocr_path  = os.path.join(base_path, "ocr_results")
    assert os.path.isdir(ocr_path)
    doc_path  = os.path.join(base_path, "documents")
    assert os.path.isdir(doc_path)
    json_file = os.path.join(base_path,f"{kind}_v1.0.json")
    assert os.path.isfile(json_file)
    base_path = os.path.abspath(base_path)
    assert os.path.isdir(base_path)

    jsDoc = _parseJson(base_path, json_file, kind)
    
    tmp:List[Dict[str, Union[docItem, ocrItem]]] = []
    
    for item in jsDoc:
        #ocr_item = _parse_ocr_json(item.ocr_path)
        tmp.append({"doc":item,"ocr":None})

    docId_list = list(set([ it['doc'].docId for it in tmp ]))
    img_path_list = list(set([ it['doc'].img_path for it in tmp ]))
    assert len(docId_list) == len(img_path_list)
    tmp_2 = {}
    tmp_3 = {}
    for it in tmp:
        imgPath = it['doc'].img_path
        docid   = it['doc'].docId
        if imgPath in tmp_2:
            tmp_2[imgPath] += [it]
        else:
            tmp_2[imgPath] = [it]
        if docid in tmp_3:
            tmp_3[docid] += [it]
        else:
            tmp_3[docid] = [it]
    assert len(tmp_2) == len(tmp_3)
    ret: List[documentQueries] = []
    for k,v in tmp_2.items():
        questions = []
        for q in v:    
            temp = questionAnswer(question=q['doc'].question,
                                  questionId=q['doc'].questionId,
                                  answers=q['doc'].answers)
            questions.append(temp)

        t = documentQueries(img_path=v[0]['doc'].img_path,
                        ocr_path=v[0]['doc'].ocr_path,
                        docId=v[0]['doc'].docId, queries=questions,
                        ocr=_parse_ocr_json(v[0]['doc'].ocr_path))
        ret.append(t)
    return ret

def create_qa_dataset(items:List[documentQueries], outJson:str, max_length:int = 512)->None:
    def clean_text(text:str)->str:
        if text is not None:
            for char in [",",".",";",":","(",")","-","/","$","%","&","*"]:
                if char in text:
                    text = text.replace(char,"")
            return text

    start = time()
    count = 0
    count_bigger = 0
    outList = []
    for i,doc in enumerate(items):
        context = " ".join(doc.ocr.text)
        for q in doc.queries:
            for ans in q.answers:
                ret = context.find(ans)
                if ret != -1 and ret < max_length:
                    count += 1
                    answer_start = ret
                    answer_end   = ret+len(ans)
                    if answer_end > max_length:
                        answer_end = max_length
                        count_bigger += 1
                    t = {
                         "img_path":doc.img_path,
                         "ocr_path":doc.img_path,
                         "docId":doc.docId,
                         "question":q.question,
                         "questionId":q.questionId,
                         "answer":ans,
                         "answer_start":answer_start,
                         "answer_end":answer_end,
                         "ocr_text":doc.ocr.text,
                         "ocr_bbox":doc.ocr.bbox,
                         "ocr_width":doc.ocr.width,
                         "ocr_height":doc.ocr.height,
                         "ocr_clockwiseOrientation":doc.ocr.clockwiseOrientation
                        }
                    outList.append(t)

    with open(outJson,"w") as fp:
        json.dump(outList,fp)

    print(f"number of items to be used:{count}")
    print(f"number of answers that were shortened to {max_length}:{count_bigger}")

    end = time()
    print(f"took {end-start} seconds")
        
    return count

class datasetQA(data.Dataset):
    
    def __init__(self, jsonFile:str, tokenizer, max_length:int,
                 qa_only:bool=True, image_size:int=1000)->None:
        assert os.path.isfile(jsonFile), f"got {jsonFile}"
        with open(jsonFile, "r") as fp:
            self.jsContent = json.load(fp)

        self.qa_only    = qa_only
        self.tokenizer  = tokenizer
        self.max_length = max_length
        self.image_size = image_size

    def __len__(self)->int:
        return len(self.jsContent)

    def _getQuestion(self, jsContent, index:int)->str:
        itemDict = self.jsContent[index]
        return itemDict["question"].lower()

    def _getAnswer(self, jsContent, index:int)->str:
        itemDict = self.jsContent[index]
        return itemDict["answer"].lower()

    def _openImage(self, img_path:str)->None:
        with open(img_path,'rb') as fp:
            img = Image.open(fp).convert('RGB')
        return img

    def _getImage(self, jsContent, index:int):
        itemDict = self.jsContent[index]
        return self._openImage(itemDict["img_path"])

    def _getOcrBBox(self, jsContent, index:int)->List[List[int]]:
        itemDict = self.jsContent[index]
        return itemDict['ocr_bbox']

    def _getOcrText(self, jsContent, index:int, join:bool=True)->List[str]:
        itemDict = self.jsContent[index]
        if join:
            ret = " ".join(itemDict['ocr_text'])
            ret = ret.lower()
        else:
            ret = [it.lower() for it in itemDict['ocr_text']]
        return ret

    def _getOcrWidth(self, jsContent, index:int)->int:
        itemDict = self.jsContent[index]
        return itemDict['ocr_width']

    def _getOcrHeight(self, jsContent, index:int)->int:
        itemDict = self.jsContent[index]
        return itemDict['ocr_height']

    def _getAnswer(self, jsContent, index:int)->Dict[str,Union[int, str]]:
        itemDict = jsContent[index]
        ret={"start":itemDict["answer_start"],
             "end":itemDict["answer_end"],
             "answer":itemDict["answer"].lower()}

        return ret

    def _sentinel_mask(self, context:str, answer:Dict[str,Union[int,str]]):
        start = answer['start']
        end   = answer['end']
        #print('start/end:',start, end)
        answer_token     = self.tokenizer.tokenize(answer["answer"])
        #print('answer_token:',answer_token, 'answer:', answer['answer'])
        sentinel_str     = " ".join(["[MASK]"]*len(answer_token))
        #print('sentine str:',sentinel_str)
        context_sentinel = context[:start]+sentinel_str+context[end:]
        return answer_token, context_sentinel
    
    def _tokenize(self,question:str, context:List[str],
                  answer:Dict[str,Union[int,str]])->None:
        answer_token, context_sentinel = self._sentinel_mask(context, answer)
        encode = self.tokenizer.encode_plus(question,
                                           context_sentinel,
                                           add_special_tokens=True,
                                           max_length=self.max_length,
                                           padding='max_length',
                                           truncation=True,
                                           return_attention_mask=True,
                                           return_tensors='pt')

        input_ids        = encode['input_ids']
        #print('inout_ids[0]:',input_ids[0], 'mask:',(input_ids[0] == self.tokenizer.mask_token_id))
        is_mask_token    = (input_ids[0] == self.tokenizer.mask_token_id)
        mask_token_index = is_mask_token.nonzero(as_tuple=False)[:,0]
        #print("mask_token_index:", mask_token_index)
        assert len(mask_token_index) == len(answer_token)
        start = mask_token_index[0]
        end   = mask_token_index[-1]
        #print('answer_token', answer_token)
        answer_token_ids = self.tokenizer.encode(answer_token,
                                                 add_special_tokens=False,
                                                 return_tensors='pt')
        #print('answer_token_ids', answer_token_ids)

        input_ids[0, start:end+1] = answer_token_ids
        #print('input_ids_with_answer_back:', input_ids)
        attention_mask            = encode['attention_mask']
        segment_ids               = encode['token_type_ids']

        return {'input_ids':input_ids.squeeze(0),
                "attn_mask":attention_mask.squeeze(0),
                "segment_ids":segment_ids.squeeze(0),
                "question":question,
                "context":context,
                "answer":answer['answer'],
                "start":start,
                "end":end}

    def _prepareImage(self, jsContent, index:int):
        image = self._getImage(jsContent, index)
        image = TF.resize(image, size=(self.image_size,self.image_size))
        image = TF.to_tensor(image).float()
        image = TF.normalize(image,[0.485, 0.456, 0.40],
                                   [0.229, 0.224, 0.225])
        return image.squeeze()

    def __getitem__(self, index:int)->None:
        assert 0<=index<len(self), f"got {index}"
        if self.qa_only:
            question = self._getQuestion(self.jsContent, index)
            answer   = self._getAnswer(self.jsContent, index)
            context  = self._getOcrText(self.jsContent, index)
            ret      = self._tokenize(question, context, answer)
            image    = self._prepareImage(self.jsContent, index)
            ret["image"] = image
            #print(ret)
            return ret

def make_dataloader_qa(tokenizer, jsonTrain:str, jsonVal:str, **kwargs):
    assert os.path.isfile(jsonTrain)
    assert os.path.isfile(jsonVal)
    assert jsonTrain != jsonVal
    dset_train = datasetQA(jsonTrain, max_length=512, tokenizer=tokenizer)
    dset_val   = datasetQA(jsonVal, max_length=512, tokenizer=tokenizer)
    dloader_train = data.DataLoader(dset_train, **kwargs)
    dloader_val   = data.DataLoader(dset_val, **kwargs)
    return {"train":dloader_train, "val":dloader_val}

def test_dataset():
    from transformers import MobileBertTokenizer, MobileBertForQuestionAnswering, MobileBertModel
    nlp_model = 'mrm8488/mobilebert-uncased-finetuned-squadv2'
    tokenizer = MobileBertTokenizer.from_pretrained(nlp_model)
    dset = datasetQA("./raw_data/mydataset_train.json", max_length=512, tokenizer=tokenizer)
    #print(dset[1023])
    dloader = data.DataLoader(dset, num_workers=8, batch_size=8)
    for i,batch in enumerate(dloader):
        print(batch['input_ids'].shape,
              batch['input_ids'].dtype,
              batch['attn_mask'].shape,
              batch['attn_mask'].dtype,
              batch["image"].shape,
              batch["image"].dtype,
              batch["image"].min(),
              batch["image"].min(),
              batch["image"].min(),
              batch["image"].max(),
              batch["image"].max(),
              batch["image"].max(),
              batch["segment_ids"].shape,
              batch['start'],
              batch['end'])
              #batch['question'],
              #batch['answer'])
        if i==5: break

if __name__ == "__main__":
    ret = buildOcrDocItems('./raw_data/train/','train')
    create_qa_dataset(ret, "./raw_data/mydataset_train.json")
    ret = buildOcrDocItems('./raw_data/val/','val')
    create_qa_dataset(ret, "./raw_data/mydataset_val.json")
    test_dataset()
