
from dataset2 import make_dataloader_qa, datasetQA
from model import dqvaModel, qaModel, qaModelBert
from metrics import compute_exact, compute_f1
from typing import List, Dict, Callable, Any, Tuple
import torch
import torch.optim as opt
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import numpy as np
import os

from transformers import T5ForConditionalGeneration, T5Tokenizer, AdamW
from transformers import get_linear_schedule_with_warmup, AdamW
from transformers import MobileBertTokenizer, MobileBertForQuestionAnswering, MobileBertModel

from transformers import AutoTokenizer, AutoModelForQuestionAnswering

def move2device(batch:Dict, device:torch.device)->Dict:
    ret = {}
    for k,v in batch.items():
        if isinstance(v, torch.Tensor):
          ret[k] = v.to(device)
        else:
            ret[k] = v  
    return ret

def train_step(batch:Dict, model:nn.Module,
               optimizer:opt.Optimizer, lr_scheduler, device:torch.device)->float:

    optimizer.zero_grad()

    batch_device = move2device(batch, device)
    # print(batch_device['input_ids'].shape,
    #       batch_device['input_ids'].device,
    #       device,
    #       batch_device['attn_mask'].shape,
    #       batch_device["segment_ids"].shape,
    #       batch_device['start'].shape,
    #       batch_device['end'].shape)
    loss = model(batch_device)
    loss.backward()
    optimizer.step()
    lr_scheduler.step()

    return loss.item()

def train_epoch(model:nn.Module, optimizer:opt.Optimizer,
                dataloader:data.DataLoader,
                lr_scheduler,
                device:torch.device)->Tuple[List[float],float]:

    loss_list:List[float] = []
    avg_loss = 0.0
    model.train()

    for i,batch in enumerate(dataloader):
        J = train_step(batch, model, optimizer, lr_scheduler, device)
        avg_loss += J
        loss_list.append(J)

    return avg_loss/float(len(dataloader)), loss_list

@torch.no_grad()
def validate_step(model:nn.Module, batch:Dict, device:torch.device, tokenizer,
                  compute_exact:Callable, compute_f1:Callable)->Dict[str,float]:
    batch_device = move2device(batch, device)
    dict_pred    = model(batch_device)
    batch_size   = float(batch_device['input_ids'].shape[0])
    answers      = dict_pred["trues"]
    preds        = dict_pred["preds"]
    exact_list   = []
    f1_list      = []

    for ans,pred in zip(answers, preds):
        exact_list.append(compute_exact(ans, pred))
        f1_list.append(compute_f1(ans, pred))

    exact:float  = sum(exact_list)/batch_size
    f1:float     = sum(f1_list)/batch_size

    return {"exact":exact, "f1":f1}


def validate_epoch(model:nn.Module,
                   dataloader:data.DataLoader,
                   compute_exact:Callable,
                   compute_f1:Callable,
                   device:torch.device,
                   epoch:int,
                   save_path:str="./",):
    model.eval()
    f1_list    = []
    exact_list = []

    for batch in dataloader:
        metrics = validate_step(model, batch, device,
                                dataloader.dataset.tokenizer,
                                compute_exact, compute_f1)

        f1_list.append(metrics['f1'])
        exact_list.append(metrics['exact'])

    avg_f1    = np.array(f1_list).mean()
    avg_exact = np.array(exact_list).mean()

    torch.save(model.state_dict(),
               os.path.join(save_path,"model_{epoch}"))

    return avg_exact, avg_f1

class attnLr2:
    def __init__(self, optimizer: opt.Optimizer,
                 warmup_start_lr: float = 1e-7,
                 warmup_end_lr: float = 6e-4,
                 warmup_steps: int = 4000) -> None:
        assert warmup_start_lr > 0, f'got {warmup_start_lr}'
        assert warmup_end_lr > 0, f'got {warmup_end_lr}'
        assert warmup_steps > 0, f'got {warmup_steps}'
        assert warmup_end_lr > warmup_start_lr
        self.a_ = (warmup_end_lr - warmup_start_lr)/float(warmup_steps)
        self.b_ = warmup_start_lr
        self.n_ = warmup_steps
        self.optimizer = optimizer
        self.step_counter = 0
        # normalize to keep lr equal to warmup_end_lr at the end of the warmup phase
        self.scale = warmup_end_lr * warmup_steps**(0.5)

    def step(self) -> None:
        self.step_counter += 1
        value = self.get_lr()
        for data in self.optimizer.param_groups:
            data['lr'] = value

    def get_lr(self, step: int = None) -> float:
        if step is None:
            step = self.step_counter
        x = step
        if x < self.n_:
            val = self.a_*x + self.b_
        else:
            val = self.scale * x**(-0.5)

        assert val >= 0
        return val

def train():
    hf_model = 'mrm8488/mobilebert-uncased-finetuned-squadv2'
    #hf_model = 'google/mobilebert-uncased'
    tokenizer = MobileBertTokenizer.from_pretrained(hf_model)
    dloaders  = make_dataloader_qa(tokenizer,
                                   "./raw_data/mydataset_train.json",
                                   "./raw_data/mydataset_val.json",
                                   num_workers=32,
                                   batch_size=32)
    train_dloader = dloaders['train']
    val_dloader   = dloaders['val']
    device        = "cuda:0"
    model         = qaModel(hf_model, tokenizer, emb_size=512)
    model         = model.to(device)

    optimizer     = opt.SGD(model.parameters(),
                             lr=1e-5, momentum=0.9,
                             weight_decay=1e-3,
                             nesterov=True)
    lr_sched = attnLr2(optimizer, warmup_end_lr=4e-4)
    for iepoch in range(15):

        avg_loss, _ = train_epoch(model, optimizer, train_dloader, lr_sched, device)
        print(f"avg_loss={avg_loss}")
        avg_exact, avg_f1 = validate_epoch(model, val_dloader,
                                    compute_exact, compute_f1,
                                    device, iepoch)
        print(f"{iepoch+1}:"
              f" avg_exact={avg_exact} avg_f1={avg_f1}")

def train_bert():
    hf_model = "bert-large-uncased-whole-word-masking-finetuned-squad"
    tokenizer = AutoTokenizer.from_pretrained(hf_model)
    dloaders  = make_dataloader_qa(tokenizer,
                                   "./raw_data/mydataset_train.json",
                                   "./raw_data/mydataset_val.json",
                                   num_workers=32,
                                   batch_size=32)
    train_dloader = dloaders['train']
    val_dloader   = dloaders['val']
    device        = "cuda:0"
    model         = qaModelBert(hf_model, tokenizer, emb_size=512)
    model         = model.to(device)

    optimizer     = opt.SGD(model.parameters(),
                             lr=1e-5, momentum=0.9,
                             weight_decay=1e-3,
                             nesterov=True)
    lr_sched = attnLr2(optimizer, warmup_end_lr=4e-4)
    for iepoch in range(15):

        avg_loss, _ = train_epoch(model, optimizer, train_dloader, lr_sched, device)
        print(f"avg_loss={avg_loss}")
        avg_exact, avg_f1 = validate_epoch(model, val_dloader,
                                    compute_exact, compute_f1,
                                    device, iepoch)
        print(f"{iepoch+1}:"
              f" avg_exact={avg_exact} avg_f1={avg_f1}")


if __name__ == "__main__":
    train()
