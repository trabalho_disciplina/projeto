
# import argparse
# import hpccm
# from hpccm.building_blocks import gnu, mlnx_ofed, openmpi, gdrcopy, knem, xpmem, pmix, ucx, slurm_pmi2, generic_autotools, packages
# from hpccm.primitives import baseimage, shell, environment

Stage0 += baseimage(image="docker.io/nvidia/cuda:11.0-cudnn8-devel-ubuntu18.04",  _as="devel")
Stage0 += mlnx_ofed(version='4.7-1.0.0.1')
Stage0 += knem(ldconfig=True)
Stage0 += gdrcopy(ldconfig=True, version="2.0")

Stage0 += packages(apt=["linux-tools-common",
                        "clang-tools",
                        "python3-dev"],
                   ospackages=["numactl",
                               "git",
                               "vim",
                               "curl",
                               "wget",
                               "pkg-config",
                               "gdb",
                               "m4"])

Stage0 += conda(channels=["pytorch"], eula=True, 
                packages=["python==3.8.5",
                          "numpy",
                          "scikit-learn",
                          "scipy",
                          "seaborn",
                          "cython",
                          "bokeh",
                          "ipython",
                          "typing",
                          "pyyaml",
                          "swig",
                          "mkl",
                          "mkl-include",
                          "swig",
                          "pip",
                          "h5py",
                          "jedi==0.17.2 ",
                          "snakeviz",
                          "jupyterlab",
                          "pandas",
                          "scons",
                          "pyarrow",
                          "distributed",
                          "pytorch",
                          "torchvision",
                          "torchaudio",  
                          "cudatoolkit=11.0",
                          "psutil",
                          "cloudpickle",
                          "tqdm",
                          "pytest",
                          "matplotlib",
                          "pybind11",
                          "nodejs"])

Stage0 += environment(variables={"HOROVOD_GPU_OPERATIONS": "NCCL"})

Stage0 += ucx(version="1.9.0",
                cuda=True, ldconfig=True,
                ofed=True,
                enable_mt=True,
                environment=True,
                pmix="/usr/local/pmix",
                gdrcopy="/usr/local/gdrcopy",
                knem="/usr/local/knem")

Stage0 += slurm_pmi2(version='17.02.11')

mpi_compiler = openmpi(cuda=True,
                    infiniband=True,
                    ldconfig=True,
                    environment=True,
                    configure_opts=["--enable-mca-no-build=btl-uct",
                                    "--enable-orterun-prefix-by-default",
                                    "--disable-getpwuid"],
                    ucx="/usr/local/ucx",
                    pmi="/usr/local/slurm-pmi2",
                    pmix="internal",
                    version='4.0.5')

Stage0 += mpi_compiler

Stage0 += cmake(eula=True, version='3.18.1')

Stage0 += packages(apt=["gcc",
                        "g++"])

Stage0 += pip(ospackages=[],
             packages=[
             "dask[complete]",
             "mlflow",
             "horovod",
             "captum",
             "fvcore",
             "gsutil",
             "jupyter_http_over_ws",
             "ipywidgets",
             "tensorboard==2.2.0",
             "jupyter-tensorboard",
             "ax-platform",
             "hyperopt",
             "geffnet",
             "efficientnet_pytorch",
             "tabulate",
             "filelock",
             "scikit-optimize",
             "allennlp",
             "gensim",
             "pytext-nlp",
             "torchtext",
             "transformers",
             "tokenizers",
             "datasets",
             "spacy",
             "nltk",
             "lmdb",       
             "einops",
             "pykeops",
             "sacrebleu",
             "pycocotools",   
             "pytorch-lightning",
             "sentence-transformers",
             "fuzzywuzzy",
             "python-Levenshtein",
             "pytorch-ignite"],
             pip="/usr/local/anaconda/bin/pip")

#Stage0 += shell(commands=["git clone https://github.com/huggingface/pytorch_block_sparse",
#                          "cd pytorch_block_sparse",
#                          "/usr/local/anaconda/bin/python setup.py install",
#                          "cd ..",
#                          "rm -rf pytorch_block_sparse"])

Stage0 += packages(apt=['curl','unzip','zlib1g-dev'])

Stage0 += shell(commands=[
"git clone https://github.com/NVIDIA/apex --depth=1",
"cd apex",
'TORCH_CUDA_ARCH_LIST="7.0;7.5" /usr/local/anaconda/bin/python setup.py install --cpp_ext --cuda_ext --bnp --deprecated_fused_adam --fast_multihead_attn --xentropy',
"cd  ..",
"rm -rf apex"
])

Stage0 += shell(commands=[
"git clone --recursive https://github.com/pytorch/fairseq",
"cd fairseq",
"/usr/local/anaconda/bin/python setup.py install",
"cd ..",
"rm -r fairseq",
])

Stage0 += shell(commands=["wget  https://github.com/bazelbuild/bazel/releases/download/3.4.1/bazel_3.4.1-linux-x86_64.deb",
                          "dpkg -i ./bazel_3.4.1-linux-x86_64.deb",
                          "apt-get install -f "])

Stage0 += shell(commands=['git clone --depth=1 https://github.com/ray-project/ray.git',
                          'export PATH=/usr/local/anaconda/bin:$PATH',
                          'cd ray',
                          'bazel build //:ray_pkg',
                          'cd python',
                          "/usr/local/anaconda/bin/pip install py_spy==0.3.0 aiohttp",
                          '/usr/local/anaconda/bin/python setup.py install',
                          'cd ../..',
                          'rm -rf ray'])

###############################################################################
# Release stage
###############################################################################
Stage1 += baseimage(image='docker.io/nvidia/cuda:11.0-cudnn8-runtime-ubuntu18.04')

Stage1 += Stage0.runtime()

Stage1 += environment(variables={"PATH": "/usr/local/anaconda/bin:${PATH}"})

Stage1 += environment(variables={"PATH": "/usr/local/anaconda/condabin:${PATH}"})

Stage1 += shell(commands=["jupyter serverextension enable --py jupyter_http_over_ws",
                          "jupyter labextension install @jupyter-widgets/jupyterlab-manager",
                          "jupyter labextension install jupyterlab_tensorboard"])

Stage1 += shell(commands=["jupyter serverextension enable --py jupyter_http_over_ws",
                          "jupyter labextension install @jupyter-widgets/jupyterlab-manager"])

Stage1 += workdir(directory="/unicamp")

Stage0 += shell(commands=["mkdir /home/unicamp_user",
                          "addgroup unicamp_user",
                          "useradd unicamp_user -s /bin/bash -g unicamp_user -d /home/unicamp_user",
                          "chown -R unicamp_user:unicamp_user /home/unicamp_user"])

