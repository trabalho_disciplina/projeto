
from dataset import docVQAsimple, make_dataloaders
from model import dqvaModel
from metrics import compute_exact, compute_f1
from typing import List, Dict, Callable, Any, Tuple
import torch
import torch.optim as opt
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
import numpy as np
import os

def move2device(batch:Dict, device:torch.device)->Dict:
    ret = {}
    for k,v in batch.items():
        if isinstance(v, torch.Tensor):
          ret[k] = v.to(device)
        else:
            ret[k] = v  
    return ret

def train_step(batch:Dict, model:nn.Module,
               optimizer:opt.Optimizer, device:torch.device)->float:

    optimizer.zero_grad()
    batch_device = move2device(batch, device)
    loss = model(batch_device)
    loss.backward()
    optimizer.step()

    return loss.item()

def train_epoch(model:nn.Module, optimizer:opt.Optimizer,
                dataloader:data.DataLoader,
                device:torch.device)->Tuple[List[float],float]:

    loss_list:List[float] = []
    avg_loss = 0.0
    model.train()

    for i,batch in enumerate(dataloader):
        J = train_step(batch, model, optimizer, device)
        avg_loss += J
        loss_list.append(J)

    return avg_loss/float(len(dataloader)), loss_list

@torch.no_grad()
def validate_step(model:nn.Module, batch:Dict, device:torch.device, tokenizer,
                  compute_exact:Callable, compute_f1:Callable)->Dict[str,float]:
    batch_device = move2device(batch, device)
    pred_ids     = model(batch_device).to("cpu")
    batch_size   = float(batch_device['image'].shape[0])
    answers      = batch['answer']
    preds        = [tokenizer.decode(ids) for ids in pred_ids]
    exact_list   = []
    f1_list      = []

    for ans,pred in zip(answers, preds):
        exact_list.append(compute_exact(ans, pred))
        f1_list.append(compute_f1(ans, pred))

    exact:float  = sum(exact_list)/batch_size
    f1:float     = sum(f1_list)/batch_size

    return {"exact":exact, "f1":f1}


def validate_epoch(model:nn.Module,
                   dataloader:data.DataLoader,
                   compute_exact:Callable,
                   compute_f1:Callable,
                   device:torch.device,
                   epoch:int,
                   save_path:str="./",):
    model.eval()
    f1_list    = []
    exact_list = []

    for batch in dataloader:
        metrics = validate_step(model, batch, device,
                                dataloader.dataset.tokenizer,
                                compute_exact, compute_f1)

        f1_list.append(metrics['f1'])
        exact_list.append(metrics['exact'])

    avg_f1    = np.array(f1_list).mean()
    avg_exact = np.array(exact_list).mean()

    torch.save(model.state_dict(),
               os.path.join(save_path,"model_{epoch}"))

    return avg_exact, avg_f1

def train():
    train_dloader = make_dataloaders("./raw_data/train", "train",
                                     pin_memory=True,shuffle=True,
                                     batch_size=64, num_workers=32)

    val_dloader   = make_dataloaders("./raw_data/val", "val",
                                     pin_memory=True,
                                     batch_size=96, num_workers=32)

    device        = "cuda:0"
    model         = dqvaModel()
    model         = model.to(device)
    optimizer     = opt.AdamW(model.parameters(), lr=1e-4)

    for iepoch in range(30):
        avg_loss, _ = train_epoch(model, optimizer, train_dloader, device)

        avg_exact, avg_f1 = validate_epoch(model, val_dloader,
                                           compute_exact, compute_f1,
                                           device, iepoch)

        print(f"{iepoch+1}:avg_loss={avg_loss}"
              f" avg_exact={avg_exact} avg_f1={avg_f1}")

if __name__ == "__main__":
    train()
