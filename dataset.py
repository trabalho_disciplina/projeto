import os
import torch
import torch.utils.data as data
from dataclasses import dataclass, field
import json
from typing import List, Dict, Callable, Any, Tuple
from torchvision.transforms.transforms import Lambda
from transformers import T5Tokenizer
from PIL import Image
import PIL
from torchvision import transforms
import matplotlib.pyplot as plt
import random

@dataclass
class docItem:
    img_path:str
    ocr_path:str
    questionId:int
    question:str
    answers:List[str] = field(default_factory=list)

@dataclass
class ocrItem:
    text:List[str]
    bbox:List[List[int]]
    width:int
    height:int
    clockwiseOrientation:float

class docVQAsimple(data.Dataset):
    def __init__(self, base_path:str, kind:str,
                 image_transforms:Callable=None,
                 model_name: str = "t5-small",
                 max_seq_length:int = 64) -> None:
        assert os.path.isdir(base_path)
        assert kind in ["train", "val", "test"] 

        self.basePath = os.path.abspath(base_path)

        self.ocrPath  = os.path.join(base_path, "ocr_results")
        assert os.path.isdir(self.ocrPath)

        self.docPath  = os.path.join(base_path, "documents")
        assert os.path.isdir(self.docPath)

        self.jsonFile = os.path.join(base_path, f"{kind}_v1.0.json")
        assert os.path.isfile(self.jsonFile)

        self.dset_items = self._parseJson(self.jsonFile, kind)

        self.image_transforms = image_transforms

        self.tokenizer = T5Tokenizer.from_pretrained(model_name)

        self.max_length = max_seq_length

    def _parseJson(self, jsonFile:str, kind:str)->List[docItem]:

        with open(jsonFile,"r") as fp:
            js = json.load(fp)

        assert js['dataset_split'] == kind
        items: List[docItem] = []
        for item in js['data']:
            t = docItem(img_path=os.path.join(self.basePath, item['image']),
                        ocr_path=os.path.join(self.basePath, item['image']\
                                   .replace(".png", ".json")\
                                   .replace("documents","ocr_results")),
                        questionId=item['questionId'],
                        question=item['question'],
                        answers=item['answers'])
            items.append(t)
        return items

    def _parse_ocr_json(self, ocr_path:str)->ocrItem:
        assert os.path.isfile(ocr_path)

        with open(ocr_path, "r") as fp:
            js = json.load(fp)

        text_list:List[str] = []
        bbox_list:List[List[int]] = []

        for it in  js['recognitionResults'][0]['lines']:
            text_list.append(it['text'])
            bbox_list.append(it['boundingBox'])

        if 'clockwiseOrientation' in js['recognitionResults'][0]:
            orientation = js['recognitionResults'][0]['clockwiseOrientation']
        else:
            orientation = 0.0

        ret = ocrItem(text=text_list,
                      bbox=bbox_list,
                      width=js['recognitionResults'][0]['width'], 
                      height=js['recognitionResults'][0]['height'],
                      clockwiseOrientation=orientation)
        return ret

    def _openImage(self, img_path:str)->None:
        with open(img_path,'rb') as fp:
            img = Image.open(fp).convert('RGB')
        return img

    def _tokenize(self, text:str, max_length:int)->Dict[str, torch.Tensor]:
        tokens = self.tokenizer(text=text,
                                max_length=max_length,
                                padding='max_length',
                                truncation=True,
                                return_tensors='pt')

        text_ids = tokens['input_ids'].squeeze(0)
        text_msk = tokens['attention_mask'].squeeze(0)

        return {'ids':text_ids, 'msk':text_msk}

    def _ocr_concatenate(self, ocr:ocrItem)->str:
        ret = ""
        n   = len(ocr.text)
        for i, line in enumerate(ocr.text):
            if i < n - 1:
                ret += line + ", "
            else:
                ret += line
        return ret

    def __len__(self) -> int:
        return len(self.dset_items)

    def __getitem__(self, item:int)->Dict[str,Any]:
        assert 0<=item<len(self)

        doc   = self.dset_items[item]
        ocr   = self._parse_ocr_json(doc.ocr_path)
        image = self._openImage(doc.img_path)

        if self.image_transforms is not None:
            image = self.image_transforms(image)

        question_tokens = self._tokenize(doc.question,
                                         self.max_length)
        answers_tokens  = self._tokenize(random.choice(doc.answers),
                                         self.max_length)
        ocr_tokens      = self._tokenize(self._ocr_concatenate(ocr),
                                         self.max_length)

        ret = {"question":doc.question,
               "question_ids":question_tokens['ids'],
               "question_msk":question_tokens['msk'],
               "answer":doc.answers[0],
               "answer_ids":answers_tokens['ids'],
               "answer_msk":answers_tokens['msk'],
               "ocr_cat_ids":ocr_tokens['ids'],
               "ocr_cat_msk":ocr_tokens['msk'],
               "image":image}

        return ret


def make_dataloaders(base_path:str, kind:str,
                     model_name: str = "t5-small",
                     distributed:bool=False,
                     max_seq_length:int = 64, **kwargs):

    mean: List[float] = [0.485, 0.456, 0.406],
    std: List[float]  = [0.229, 0.224, 0.225],

    image_transforms = transforms.Compose([
                                 transforms.Resize((740, 560)),
                                 transforms.ToTensor(),
                                 transforms.Lambda(lambda x:x.permute(1,2,0).contiguous()),
                                 transforms.Normalize(mean, std),
                                 transforms.Lambda(lambda x:x.permute(2,0,1).contiguous()),
                                        ])

    dset = docVQAsimple(base_path, kind=kind,
                        image_transforms=image_transforms,
                        model_name=model_name,
                        max_seq_length=max_seq_length)
    if distributed:
        dloader = data.DataLoader(dset,
                                  sampler=data.distributed.DistributedSampler(dset),
                                  **kwargs)
    else:
        dloader = data.DataLoader(dset,**kwargs)

    return dloader

def run():
    dset = docVQAsimple("./raw_data/train", "train")
    print(dset.basePath, dset.docPath, dset.jsonFile)
    print(len(dset))
    print(dset[10])
    print(dset[1000])
    dloader = make_dataloaders("./raw_data/train", "train", batch_size=16, num_workers=32)
    for i,j in enumerate(dloader):
        print(j['image'].shape)
        if i%10 == 0:
            plt.imshow(j['image'].detach().numpy()[0].transpose(1,2,0))
            plt.savefig(f'img_{i}.png')
            plt.close()

if __name__ == "__main__":
    run()


